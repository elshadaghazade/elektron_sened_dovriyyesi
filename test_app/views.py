from django.shortcuts import render
from django.views.generic import ListView, CreateView, UpdateView, TemplateView
from django.urls import reverse_lazy

from .models import Publisher

class PublisherList(ListView):
    model = Publisher
    template_name='publisher_list.html'
    context_object_name = 'publishers'

class PublisherCreate(CreateView):
    fields = 'name', 'address', 'city', 'state_province', 'country', 'website'
    model = Publisher
    template_name = 'publisher_create.html'
    


class PublisherEdit(UpdateView):
    model = Publisher
    fields = 'name', 'address', 'city', 'state_province', 'country', 'website'
    template_name = 'publisher_edit.html'
    success_url = reverse_lazy('publisher_list')

    def form_valid(self, form):
        form.instance.name = str(self.request.user.id)

        return super().form_valid(form)