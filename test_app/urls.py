from django.urls import path
from .views import *

urlpatterns = [
    path('publishers/', PublisherList.as_view(), name='publisher_list'),
    path('publishers/create/', PublisherCreate.as_view(), name='publisher_create'),
    path('publishers/<pk>/', PublisherEdit.as_view(), name='publisher_edit')
]
