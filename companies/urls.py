from django.urls import path
from .views import *

urlpatterns = [
    path('', CompanyView.as_view(), name='company_view'),
    path('create_asan_credential/', CreateCompanyAsanCredential.as_view(), name='create_asan_credential'),
    path('update_asan_credential/<pk>/', UpdateCompanyAsanCredential.as_view(), name='update_asan_credential'),
    path('remove_asan_credential/<pk>/', CompanyAsanCredentialRemove.as_view(), name='remove_asan_credential'),
    path('create_document/', CreateDocument.as_view(), name='create_document'),
    path('update/', UpdateCompany.as_view(), name='update_company'),
]
