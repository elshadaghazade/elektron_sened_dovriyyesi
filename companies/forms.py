from .models import Company
from django import forms


class UpdateCompanyForm(forms.ModelForm):
    class Meta:
        model = Company
        fields = ['voen', 'company_name', 'ceo_first_name', 'ceo_last_name']
        exclude = []