from django.contrib import admin
from .models import *

admin.site.register(Company, CompanyAdmin)
admin.site.register(CompanyContact, CompanyContactAdmin)
admin.site.register(CompanyAsanCredential, CompanyAsanCredentialAdmin)