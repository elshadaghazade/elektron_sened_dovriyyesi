from django.shortcuts import render
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django import forms
from .models import *
from documents.models import *
from django.views.generic import TemplateView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from datetime import datetime

class UpdateCompany(SuccessMessageMixin, UpdateView):
    model = Company
    template_name = 'update_company.html'
    fields = ['voen', 'company_name', 'ceo_first_name', 'ceo_last_name']
    success_url = reverse_lazy('company_view')

    def get_success_message(self, cleaned_data):
        messages.success(self.request, "Company was updated successfully")

    def get_object(self):
        return Company.objects.get(pk=self.request.user)


class CreateCompanyAsanCredential(CreateView):
    model = CompanyAsanCredential
    template_name = 'create_company_asan_credential.html'
    fields = ['asan_number', 'asan_id']
    success_url = reverse_lazy('company_view')


    def form_valid(self, form):
        form.instance.company = Company.objects.get(user=self.request.user)
        return super().form_valid(form)


class UpdateCompanyAsanCredential(SuccessMessageMixin, UpdateView):
    """bu view asan imzani deyishir"""
    model = CompanyAsanCredential
    template_name = 'update_company_asan_credential.html'
    fields = ['asan_number', 'asan_id']
    success_url = reverse_lazy('company_view')

    def get_success_message(self, form):

        messages.success(self.request, "Asan imza ugurla deyishdirildi")

    def get_queryset(self):
        qs = super().get_queryset()

        return qs.filter(company=Company.objects.get(user=self.request.user))













# class CreateCompanyAsanCredential(SuccessMessageMixin, CreateView):
#     model = CompanyAsanCredential
#     template_name = 'create_company_asan_credential.html'
#     fields = ['asan_id', 'asan_number']
#     success_url = reverse_lazy('company_view')

#     def get_success_message(self, cleaned_data):
#         messages.success(self.request, "ASAN Credential was created successfully")

#     def form_valid(self, form):
#         form.instance.company = Company.objects.get(pk=self.request.user)

#         return super().form_valid(form)

# class UpdateCompanyAsanCredential(SuccessMessageMixin, UpdateView):
#     model = CompanyAsanCredential
#     template_name = 'update_company_asan_credential.html'
#     fields = ['asan_id', 'asan_number']
#     success_url = reverse_lazy('company_view')

#     def get_success_message(self, cleaned_data):
#         messages.success(self.request, 'ASAN Credential was changed successfully')


class CreateDocument(SuccessMessageMixin, CreateView):
    model = Documents
    template_name = 'create_document.html'
    fields = ['receiver', 'notes', 'file', 'doc_date', 'doc_exp_date']
    success_url = reverse_lazy('company_view')

    # def get_form(self):
    #     form = super(CreateDocument, self).get_form()
    #     # form.fields['doc_date'].widget.attrs.update({
    #     #     'class': 'form-control datetimepicker-input',
    #     #     'data-target': '#datetimepicker1'
    #     # })
    #     return form

    def form_valid(self, form):
        form.instance.sender = self.request.user
        form.instance.status = Documents.STATUS_CREATED
        return super(CreateDocument, self).form_valid(form)


    def get_success_message(self, form):
        messages.success(self.request, 'Document was created successfully')


class CompanyAsanCredentialRemove(SuccessMessageMixin, DeleteView):
    model = CompanyAsanCredential
    template_name = 'remove_company_asan_credential.html'
    success_url = reverse_lazy('company_view')

    def get_success_message(self, form):
        messages.success(self.request, 'Asan imza müvəffəqiyyətlə silindi')


class CompanyView(TemplateView):
    model = Company
    template_name = 'company_view.html'

    def get_context_data(self):

        context = super().get_context_data()
        company = Company.objects.get(user=self.request.user)
        sent_documents = company.sender_set.all()
        for doc in sent_documents:
            doc.formatted_doc_date = doc.doc_date.strftime("%d %b, %Y")
            doc.formatted_doc_exp_date = doc.doc_exp_date.strftime("%d %b, %Y")
            if doc.doc_exp_date.timestamp() < datetime.now().timestamp():
                doc.status = Documents.STATUS_EXPIRED
                doc.save()

        context['company'] = company
        context['messages'] = messages.get_messages(self.request)
        context['asan_imzalar'] = company.companyasancredential_set.all()
        context['sent_documents'] = sent_documents
        context['received_documents'] = company.receiver_set.all()
        context['document_class'] = Documents
        return context














# class CompanyView(TemplateView):
#     model = Company
#     template_name = 'company_view.html'

#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['messages'] = messages.get_messages(self.request)
#         context['company'] = Company.objects.get(user=self.request.user)
#         # context['documents'] = Company.objects.filter(sender__eq=context['company']).receiver_set.all()
#         # print(context['company'].receiver_set)

#         return context