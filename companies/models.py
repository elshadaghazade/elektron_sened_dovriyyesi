from django.db import models
from django.contrib.auth.models import User
from django.contrib import admin
from django.shortcuts import redirect
from django.urls import reverse_lazy

class Company(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    voen = models.CharField("VÖEN", max_length=20, unique=True)
    company_name = models.CharField("Şirkətin adı", max_length=255)
    ceo_first_name = models.CharField("Rəhbərin adı", max_length=60)
    ceo_last_name = models.CharField("Rəhbərin soyadı",max_length=60)
    registered_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.company_name

    # def get_absolute_url(self):
    #     return reverse_lazy("company_view")
    

    class Meta:
        verbose_name = 'Şirkət'
        verbose_name_plural = 'Şirkətlər'


class CompanyAsanCredential(models.Model):
    company = models.ForeignKey(Company, verbose_name="Şirkət", on_delete=models.CASCADE)
    asan_number = models.CharField("ASAN nömrə", max_length=20, unique=True)
    asan_id = models.CharField("ASAN ID", max_length=20, unique=True)

    def __str__(self):
        return f"Asan nömrə: {self.asan_number} | Asan ID: {self.asan_id}"

    class Meta:
        verbose_name = "ASAN İmza"
        verbose_name_plural = "Şirkətin ASAN imzaları"

class CompanyContact(models.Model):
    CONTACT_TYPE_PHONE = 1
    CONTACT_TYPE_EMAIL = 2
    CONTACT_TYPE_ADDRESS = 3

    choices = (
        (CONTACT_TYPE_PHONE, 'Telefon'),
        (CONTACT_TYPE_EMAIL, 'Email'),
        (CONTACT_TYPE_ADDRESS, 'Adres'),
    )

    company = models.ForeignKey(Company, verbose_name="Şirkət", on_delete=models.CASCADE)
    contact_type = models.IntegerField("Əlaqə növü", choices=choices)
    contact = models.CharField("Əlaqə", max_length=255)

    def __str__(self):
        type = list(filter(lambda x: x[0] == self.contact_type, self.choices))[0][1]
        return f"{type}: {self.contact}"

    class Meta:
        verbose_name = "Əlaqə"
        verbose_name_plural = "Şirkətin Əlaqə Məlumatları"


###### admin #######
class CompanyAsanCredentialAdmin(admin.ModelAdmin):
    exclude = 'company',

    def save_model(self, request, obj, form, change):
        obj.company = Company.objects.get(user=request.user)

        return super(CompanyAsanCredentialAdmin, self).save_model(request, obj, form, change)

    def get_queryset(self, request):
        qs = super(CompanyAsanCredentialAdmin, self).get_queryset(request)

        if request.user.is_superuser:
            return qs

        return qs.filter(company=Company.objects.get(pk=request.user))

class CompanyAdmin(admin.ModelAdmin):
    exclude = 'user', 'registered_at'

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        return super(CompanyAdmin, self).save_model(request, obj, form, change)

    def get_queryset(self, request):
        qs = super(CompanyAdmin, self).get_queryset(request)

        if request.user.is_superuser:
            return qs

        return qs.filter(user=request.user)
    

class CompanyContactAdmin(admin.ModelAdmin):
    exclude = "company",

    def save_model(self, request, obj, form, change):
        obj.company = Company.objects.get(user=request.user)

        super(CompanyContactAdmin, self).save_model(request, obj, form, change)

    def get_queryset(self, request):
        qs = super(CompanyContactAdmin, self).get_queryset(request)

        if request.user.is_superuser:
            return qs

        return qs.filter(company=Company.objects.get(pk=request.user))

    # def formfield_for_foreignkey(self, db_field, request, **kwargs):
        
    #     if db_field.name == "category_id":
    #         kwargs["queryset"] = Category.objects.filter(profile=request.user)

    #     return super().formfield_for_foreignkey(db_field, request, **kwargs)