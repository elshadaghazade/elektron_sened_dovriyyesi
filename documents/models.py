from django.db import models
from django.contrib.auth.models import User
from companies.models import Company

class Documents(models.Model):
    STATUS_CREATED = 1
    STATUS_SIGNED_BY_SENDER = 2
    STATUS_SIGNED_BY_RECEIVER = 3
    STATUS_REJECTED = 4
    STATUS_EXPIRED = 5

    sender = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='sender_set')
    receiver = models.ForeignKey(Company, models.CASCADE, related_name='receiver_set')
    notes = models.CharField(max_length=255, null=True)
    file = models.FileField(upload_to='media')
    # hash = models.CharField(max_length=32)
    doc_date = models.DateTimeField()
    doc_exp_date = models.DateTimeField()
    status = models.IntegerField(choices=(
        (STATUS_CREATED, 'Hazırlandı'),
        (STATUS_SIGNED_BY_SENDER, "Göndərilən tərəfindən təsdiqləndi"),
        (STATUS_SIGNED_BY_RECEIVER, "Qəbul edən tərəfindən imzalandı"),
        (STATUS_REJECTED, "İmtina edildi")
    ), default=STATUS_CREATED)

    created_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"from: {self.sender.username} to: {self.receiver.username} | {self.notes}"

    class Meta:
        verbose_name = "Sənəd"
        verbose_name_plural = "Sənədlər"