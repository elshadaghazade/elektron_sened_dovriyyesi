# Generated by Django 2.1.5 on 2019-02-08 15:58

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Documents',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('notes', models.CharField(max_length=255, null=True)),
                ('file', models.FileField(upload_to='media')),
                ('doc_date', models.DateTimeField()),
                ('doc_exp_date', models.DateTimeField()),
                ('status', models.IntegerField(choices=[(1, 'Hazırlandı'), (2, 'Göndərilən tərəfindən təsdiqləndi'), (3, 'Qəbul edən tərəfindən imzalandı')])),
                ('created_at', models.DateTimeField(auto_now=True)),
                ('receiver', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='receiver2receiver', to=settings.AUTH_USER_MODEL)),
                ('sender', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sender2sender', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
