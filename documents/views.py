from django.shortcuts import render
from django.views.generic import CreateView, UpdateView, DeleteView
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from .models import *
from django.urls import reverse_lazy


class DocumentCreate(SuccessMessageMixin, CreateView):
    model = Documents
    template_name = 'create_document.html'
    fields = ['receiver', 'notes', 'file', 'doc_date', 'doc_exp_date']
    success_url = reverse_lazy('company_view')

    def form_valid(self, form):
        form.instance.sender = Company.objects.get(user=self.request.user)
        return super().form_valid(form)