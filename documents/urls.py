from django.urls import path
from .views import *


urlpatterns = [
    path('create/', DocumentCreate.as_view(), name='document_create')
]
